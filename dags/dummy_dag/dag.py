"""
Throwaway testing DAG that we will be removing once
Constellation is productionized.
"""


from datetime import datetime

from airflow import DAG
from airflow.decorators import task
from airflow.providers.snowflake.operators.snowflake import SnowflakeOperator

SF_ROLE = "SNOWFLAKE_DATA_OS"
SF_WAREHOUSE = "RESERVED_DATA_OS_WAREHOUSE"


with DAG(
    "hello_world_dag",
    schedule_interval="*/1 * * * *",
    start_date=datetime(2022, 1, 20),
    catchup=False,
) as dag:

    @task(task_id="hello_world_task")
    def print_hello_world():
        print("Hello world")
        return "Hello world return statement"

    test_airflow_task = SnowflakeOperator(
        task_id="test_airflow_task",
        snowflake_conn_id="snowflake_db",
        sql="SELECT SFDC_ENTITY_ID FROM DEV_ROBLEE.MARTS_DATASTAGE.KEYHOLE_SFDC_CPM_ATTRIBUTES LIMIT 2",
        database="dev_roblee",
        warehouse=SF_WAREHOUSE,
        role=SF_ROLE,
    )

    hello_world_task = print_hello_world()

    hello_world_task >> test_airflow_task
